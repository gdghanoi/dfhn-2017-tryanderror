package skay.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import skay.model.tbl_oder;

@Repository
public interface OderRepository extends CrudRepository<tbl_oder, Integer> {
}
