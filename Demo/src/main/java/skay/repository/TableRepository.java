package skay.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import skay.model.tbl_table;

@Repository
public interface TableRepository extends CrudRepository<tbl_table, Integer>{
	
}
