package skay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import skay.model.tbl_dish;

@Repository
public interface DishRepository extends CrudRepository<tbl_dish, Integer> {
	
	@Query(value = "select dish_id, dish_name, CONCAT(:host,url_image) as url_image , price"
			+ " from tbl_dish ORDER BY dish_id asc limit :from,:to", nativeQuery = true)
	List<tbl_dish> findEndWith(@Param("host") String host, @Param("from") int from, @Param("to") int to);
}
