package skay.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import skay.model.tbl_user;

@Repository
public interface UserRepository extends CrudRepository<tbl_user, Integer> {
	// login
	List<tbl_user> findByUsernameAndPassword(String username, String pass);
	
	// find by user name
	tbl_user findByUsername(String username);
}
