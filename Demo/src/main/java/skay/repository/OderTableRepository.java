package skay.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import skay.model.tbl_oder_table;

@Repository
public interface OderTableRepository extends CrudRepository<tbl_oder_table, Integer> {
	@Query(value = "select id, date_time_oder, table_id, user_id, oder_id, date_system"
			+ " from tbl_oder_table ORDER BY id desc limit :from,:to", nativeQuery = true)
	List<tbl_oder_table> findEndWith(@Param("from") int from, @Param("to") int to);
}
