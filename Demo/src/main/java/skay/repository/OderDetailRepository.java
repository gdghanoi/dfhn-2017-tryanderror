package skay.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import skay.model.tbl_oder_detail;

@Service
public interface OderDetailRepository extends CrudRepository<tbl_oder_detail, Integer> {

}
