package skay.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import api_tryanderror.API_PageCustom;
import skay.model.api_GetListDish;
import skay.model.tbl_dish;
import skay.service.DishService;

@RestController
public class DishController {
	@Autowired
	DishService dishService;

	@RequestMapping(value = "/getListDish", method = RequestMethod.POST)
	public api_GetListDish getListDish(HttpServletRequest request, HttpServletResponse response, Model model) {
		// get param
		String index = request.getParameter("index");
		String limit = request.getParameter("limit");

		API_PageCustom page = new API_PageCustom(index, limit);

		List<tbl_dish> result = dishService.getListDish(page.fromLimit(), page.toLimit());

		int indexNext = page.indexNext(result.size(), index);

		if (indexNext != -1 && !index.equals("0") && !index.equals("1")) {
			result.remove(result.size() - 1);
		}
		if (result.size() == 0 || result == null) {
			return new api_GetListDish(2, GMessage(2), indexNext, null);
		} else {
			return new api_GetListDish(1, GMessage(1), indexNext, result);
		}
	}

	public String GMessage(int status) {
		if (status == 1) {
			return "Thành công";
		} else {
			return "Lỗi";
		}
	}
}
