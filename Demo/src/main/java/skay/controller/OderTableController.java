package skay.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import api_tryanderror.API_PageCustom;
import skay.model.api_GetListOderTable;
import skay.model.api_StatusAndMess;
import skay.model.tbl_oder_table;
import skay.model.tbl_table;
import skay.service.OderTableService;
import skay.service.TableService;

@RestController
public class OderTableController {
	@Autowired
	OderTableService oderTableService;

	@Autowired
	TableService tableService;

	@RequestMapping(value = "/getListOderTable", method = RequestMethod.POST)
	public api_GetListOderTable getListOderTable(HttpServletRequest request, HttpServletResponse response,
			Model model) {
		// get param
		String index = request.getParameter("index");
		String limit = request.getParameter("limit");

		API_PageCustom page = new API_PageCustom(index, limit);

		List<tbl_oder_table> result = oderTableService.getListOderTable(page.fromLimit(), page.toLimit());

		int indexNext = page.indexNext(result.size(), index);

		if (indexNext != -1 && !index.equals("0") && !index.equals("1")) {
			result.remove(result.size() - 1);
		}
		if (result.size() == 0 || result == null) {
			return new api_GetListOderTable(2, GMessage(2), indexNext);
		} else {
			return new api_GetListOderTable(1, GMessage(1), indexNext, result);
		}
	}

	@RequestMapping(value = "/createOderTable", method = RequestMethod.POST)
	public api_StatusAndMess createOderTable(HttpServletRequest request, HttpServletResponse response, Model model) {
		// get param
		String datetime = request.getParameter("datetime");
		int tableid = Integer.parseInt(request.getParameter("tableid"));
		int userid = Integer.parseInt(request.getParameter("userid"));

		tbl_table tb = tableService.findOne(tableid);
		if (tb.getCheck() != 2) {
			return new api_StatusAndMess(2, GMessage(2));
		} else {
			tbl_oder_table od = new tbl_oder_table(datetime, tableid, userid);
			if (oderTableService.save(od)) {
				return new api_StatusAndMess(1, GMessage(1));
			}
			return new api_StatusAndMess(2, GMessage(2));
		}
	}

	public String GMessage(int status) {
		if (status == 1) {
			return "Thành công";
		} else {
			return "Lỗi";
		}
	}
}
