package skay.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import skay.model.api_StatusAndMess;
import skay.model.tbl_dish;
import skay.model.tbl_oder;
import skay.model.tbl_oder_detail;
import skay.service.DishService;
import skay.service.OderDetailService;
import skay.service.OderService;

@RestController
public class OderController {
	@Autowired
	OderService oderService;

	@Autowired
	DishService dishService;

	@Autowired
	OderDetailService oderDetailService;

	@RequestMapping(value = "/createOder", method = RequestMethod.POST)
	public api_StatusAndMess checktable(HttpServletRequest request, HttpServletResponse response, Model model) {

		String date = request.getParameter("date");
		String dishCount = request.getParameter("dishcount");
		int userid = Integer.parseInt(request.getParameter("userid"));
		int tableid = Integer.parseInt(request.getParameter("tableid"));
		
		try {
			tbl_oder o = new tbl_oder(userid, tableid, date, 0);
			oderService.save(o);

			int price = 0;

			String[] lstDishCount = dishCount.split("-");
			for (int i = 0; i < lstDishCount.length; i++) {
				String[] temp = lstDishCount[i].split(",");
				int dishid = Integer.parseInt(temp[0]);
				int count = Integer.parseInt(temp[1]);
				tbl_dish d = dishService.findOne(dishid);
				int pr = count * d.getPrice();
				System.out.println(dishid + " - " + count + " - " + pr);
				tbl_oder_detail od = new tbl_oder_detail(o.getOderid(), d.getDishid(), count, pr);
				oderDetailService.save(od);
				price += pr;
			}
			o.setPrice(price);
			oderService.save(o);
			return new api_StatusAndMess(1, GMessage(1));
		} catch (Exception e) {
			return new api_StatusAndMess(2, GMessage(2));
		}

	}

	public String GMessage(int status) {
		if (status == 1) {
			return "Thành công";
		} else {
			return "Lỗi";
		}
	}

}
