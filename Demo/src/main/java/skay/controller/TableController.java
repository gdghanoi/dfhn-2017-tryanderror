package skay.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import skay.model.api_StatusAndMess;
import skay.model.api_table;
import skay.model.tbl_table;
import skay.service.TableService;

@RestController
public class TableController {
	@Autowired
	TableService tableService;

	@RequestMapping(value = "/getAllTable", method = RequestMethod.POST)
	public api_table getall() {
		List<tbl_table> lstTbl = tableService.getAllTBL();
		if (lstTbl == null) {
			return new api_table(2, GMessage(2));
		}
		return new api_table(1, GMessage(1), lstTbl);
	}
	
	@RequestMapping(value = "/changeInforTable", method = RequestMethod.POST)
	public api_StatusAndMess changeInforTable(HttpServletRequest request, HttpServletResponse response, Model model) {
		int id = Integer.parseInt(request.getParameter("idtable"));
		int check = Integer.parseInt(request.getParameter("check"));
		
		tbl_table tbl = tableService.findOne(id);
		tbl.setCheck(check);
		if(tableService.save(tbl)) {
			return new api_StatusAndMess(1, GMessage(1));
		}
		return new api_StatusAndMess(2, GMessage(2));
	}
	
	@RequestMapping(value = "/checkTable", method = RequestMethod.POST)
	public api_StatusAndMess checktable(HttpServletRequest request, HttpServletResponse response, Model model) {
		int id = Integer.parseInt(request.getParameter("tableid"));
		
		tbl_table tbl = tableService.findOne(id);
		if(tbl.getCheck() == 2) {
			return new api_StatusAndMess(1, GMessage(1));
		}
		return new api_StatusAndMess(2, GMessage(2));
	}
	
	public String GMessage(int status) {
		if (status == 1) {
			return "Thành công";
		} else {
			return "Lỗi";
		}
	}
}
