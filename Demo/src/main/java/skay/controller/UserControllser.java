package skay.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import skay.model.api_Login;
import skay.model.api_StatusAndMess;
import skay.model.tbl_user;
import skay.service.UserService;

@RestController
public class UserControllser {
	@Autowired
	UserService userService;

	@RequestMapping(value = "/getall", method = RequestMethod.POST)
	public List<tbl_user> getall() {
		return userService.findAll();
	}

	@RequestMapping(value = "/t", method = RequestMethod.GET)
	public String getall1() {
		return "hi";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public api_Login loginByUserAndPass(HttpServletRequest request, HttpServletResponse response, Model model) {
		// get parameter
		String name = request.getParameter("username");
		String pwd = request.getParameter("pass");

		System.out.println("login - " + name + " - " + pwd);

		List<tbl_user> lstLogin = userService.login(name, pwd);
		tbl_user user = userService.CheckUserHave(name);
		if (lstLogin.size() == 0) {
			if (user == null) {
				return new api_Login(2, LoginMessage(2));
			} else {
				return new api_Login(3, LoginMessage(3));
			}
		} else {
			return new api_Login(1, LoginMessage(1), user);
		}
	}
	
	@RequestMapping(value = "/changepass", method = RequestMethod.POST)
	public api_StatusAndMess userChangePass(HttpServletRequest request, HttpServletResponse response, Model model) {
		Integer userid = Integer.parseInt(request.getParameter("userid"));
		String newpass = request.getParameter("newpass");

		System.out.println("user change pass api - " + userid + " - " + newpass);

		tbl_user u = userService.findOne(userid);
		u.setPassword(newpass);

		try {
			userService.save(u);
			return new api_StatusAndMess(1, GMessage(1));
		} catch (Exception e) {
			System.out.println(e.toString());
			return new api_StatusAndMess(2, GMessage(2));
		}
	}

	public String GMessage(int status) {
		if (status == 1) {
			return "Thành công";
		} else {
			return "Lỗi";
		}
	}
	
	public String LoginMessage(int status) {
		if (status == 1) {
			return "Login thành công";
		} else if (status == 2) {
			return "Không tồn tại user";
		} else {
			return "Mật khẩu không đúng";
		}
	}
}
