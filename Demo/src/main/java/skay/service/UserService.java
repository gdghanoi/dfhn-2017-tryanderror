package skay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import skay.model.tbl_user;
import skay.repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;
	
	public List<tbl_user> findAll(){
		return (List<tbl_user>) userRepository.findAll();
	}
	
	public List<tbl_user> login(String username, String pass) {
		return userRepository.findByUsernameAndPassword(username, pass);
	}
	
	public tbl_user CheckUserHave(String user) {
		return userRepository.findByUsername(user);
	}
	
	public tbl_user findOne(int id) {
		return userRepository.findOne(id);
	}
	
	public void save(tbl_user u) {
		userRepository.save(u);
	}
}
