package skay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import skay.model.tbl_oder_table;
import skay.repository.OderTableRepository;

@Service
public class OderTableService {
	@Autowired
	OderTableRepository oderTableRepository;

	public List<tbl_oder_table> getAll() {
		return (List<tbl_oder_table>) oderTableRepository.findAll();
	}

	public List<tbl_oder_table> getListOderTable(int from, int to) {
		return oderTableRepository.findEndWith(from, to);
	}

	public boolean save(tbl_oder_table tbl) {
		try {
			oderTableRepository.save(tbl);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
