package skay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import skay.model.tbl_oder_detail;
import skay.repository.OderDetailRepository;

@Service
public class OderDetailService {
	@Autowired
	OderDetailRepository oderDetailRepository;
	
	public void save(tbl_oder_detail od) {
		oderDetailRepository.save(od);
	}
}
