package skay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import skay.model.tbl_oder;
import skay.repository.OderRepository;

@Service
public class OderService {
	@Autowired
	OderRepository oderRepository;
	
	public boolean save(tbl_oder o) {
		try {
			oderRepository.save(o);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public tbl_oder getOne(int id) {
		return oderRepository.findOne(id);
	}
}
