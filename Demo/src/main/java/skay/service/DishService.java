package skay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import api_tryanderror.API_Host;
import skay.model.tbl_dish;
import skay.repository.DishRepository;

@Service
public class DishService {
	@Autowired
	DishRepository dishRepository;
	
	public List<tbl_dish> getListDish(int from, int to){
		return dishRepository.findEndWith(API_Host.getLinkDish(), from, to);
	}
	
	public tbl_dish findOne(int id) {
		return dishRepository.findOne(id);
	}

}
