package skay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import skay.model.tbl_table;
import skay.repository.TableRepository;

@Service
public class TableService {
	@Autowired
	TableRepository tableRepository;

	public List<tbl_table> getAllTBL() {
		return (List<tbl_table>) tableRepository.findAll();
	}

	public boolean save(tbl_table t) {
		tbl_table table = tableRepository.save(t);
		if (table == null) {
			return false;
		} else {
			return true;
		}
	}
	
	public tbl_table findOne(int id) {
		return tableRepository.findOne(id);
	}

}
