package skay.model;

public class api_StatusAndMess {
	private int status;
	private String message;

	public api_StatusAndMess() {
		super();
	}

	public api_StatusAndMess(int status, String message) {
		super();
		this.status = status;
		this.message = message;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
