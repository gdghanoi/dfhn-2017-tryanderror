package skay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_user")
public class tbl_user {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_id")
	private Integer userid;

	@Column(name = "user_name")
	private String username;

	private String password;
	private String name;
	private String phone;
	private int gender;
	private int role;

	public tbl_user() {
		super();
	}

	public tbl_user(String username, String password, String name, String phone, int gender, int role) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.phone = phone;
		this.gender = gender;
		this.role = role;
	}

	public tbl_user(Integer userid, String username, String password, String name, String phone, int gender, int role) {
		super();
		this.userid = userid;
		this.username = username;
		this.password = password;
		this.name = name;
		this.phone = phone;
		this.gender = gender;
		this.role = role;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

}
