package skay.model;

public class api_Login {
	private int status;
	private String message;
	private tbl_user user;

	public api_Login(int status, String message, tbl_user user) {
		super();
		this.status = status;
		this.message = message;
		this.user = user;
	}

	public api_Login(int status, String message) {
		super();
		this.status = status;
		this.message = message;
		this.user = null;
	}
	
	public api_Login() {
		super();
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public tbl_user getUser() {
		return user;
	}

	public void setUser(tbl_user user) {
		this.user = user;
	}

}
