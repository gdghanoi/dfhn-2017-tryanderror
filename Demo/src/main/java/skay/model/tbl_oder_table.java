package skay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_oder_table")
public class tbl_oder_table {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "date_time_oder")
	private String dateTimeOder;

	@Column(name = "table_id")
	private Integer tableid;

	@Column(name = "user_id")
	private Integer userid;

	@Column(name = "oder_id")
	private Integer oderid;

	@Column(name = "date_system")
	private String datesystem;

	public tbl_oder_table() {
		super();
	}

	public tbl_oder_table(Integer id, String dateTimeOder, Integer tableid, Integer userid, Integer oderid,
			String datesystem) {
		super();
		this.id = id;
		this.dateTimeOder = dateTimeOder;
		this.tableid = tableid;
		this.userid = userid;
		this.oderid = oderid;
		this.datesystem = datesystem;
	}

	public tbl_oder_table(String dateTimeOder, Integer tableid, Integer userid) {
		super();
		this.dateTimeOder = dateTimeOder;
		this.tableid = tableid;
		this.userid = userid;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDateTimeOder() {
		return dateTimeOder;
	}

	public void setDateTimeOder(String dateTimeOder) {
		this.dateTimeOder = dateTimeOder;
	}

	public Integer getTableid() {
		return tableid;
	}

	public void setTableid(Integer tableid) {
		this.tableid = tableid;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getOderid() {
		return oderid;
	}

	public void setOderid(Integer oderid) {
		this.oderid = oderid;
	}

	public String getDatesystem() {
		return datesystem;
	}

	public void setDatesystem(String datesystem) {
		this.datesystem = datesystem;
	}

}
