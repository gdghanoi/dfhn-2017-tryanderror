package skay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_oder_detail")
public class tbl_oder_detail {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "oder_detail_id")
	private Integer oder_detail_id;

	@Column(name = "oder_id")
	private Integer oderid;

	@Column(name = "dish_id")
	private Integer dishid;

	private Integer count;
	private Integer price;

	public tbl_oder_detail() {
		super();
	}

	public tbl_oder_detail(Integer oder_detail_id, Integer oderid, Integer dishid, Integer count, Integer price) {
		super();
		this.oder_detail_id = oder_detail_id;
		this.oderid = oderid;
		this.dishid = dishid;
		this.count = count;
		this.price = price;
	}

	public tbl_oder_detail(Integer oderid, Integer dishid, Integer count, Integer price) {
		super();
		this.oderid = oderid;
		this.dishid = dishid;
		this.count = count;
		this.price = price;
	}

	public Integer getOder_detail_id() {
		return oder_detail_id;
	}

	public void setOder_detail_id(Integer oder_detail_id) {
		this.oder_detail_id = oder_detail_id;
	}

	public Integer getOderid() {
		return oderid;
	}

	public void setOderid(Integer oderid) {
		this.oderid = oderid;
	}

	public Integer getDishid() {
		return dishid;
	}

	public void setDishid(Integer dishid) {
		this.dishid = dishid;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

}
