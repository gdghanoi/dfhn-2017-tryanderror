package skay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_oder")
public class tbl_oder {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "oder_id")
	private Integer oderid;

	@Column(name = "user_id")
	private Integer userid;

	@Column(name = "table_id")
	private Integer tableid;
	
	@Column(name = "oder_date")
	private String oderdate;

	private Integer price;

	public tbl_oder() {
		super();
	}

	public tbl_oder(Integer userid, Integer tableid, String oderdate, Integer price) {
		super();
		this.userid = userid;
		this.tableid = tableid;
		this.oderdate = oderdate;
		this.price = price;
	}

	public Integer getOderid() {
		return oderid;
	}

	public void setOderid(Integer oderid) {
		this.oderid = oderid;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getTableid() {
		return tableid;
	}

	public void setTableid(Integer tableid) {
		this.tableid = tableid;
	}

	public String getOderdate() {
		return oderdate;
	}

	public void setOderdate(String oderdate) {
		this.oderdate = oderdate;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}
}
