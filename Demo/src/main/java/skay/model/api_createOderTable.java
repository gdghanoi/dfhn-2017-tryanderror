package skay.model;

public class api_createOderTable {
	private int status;
	private String message;
	private tbl_oder_table table;

	public api_createOderTable() {
		super();
	}

	public api_createOderTable(int status, String message, tbl_oder_table table) {
		super();
		this.status = status;
		this.message = message;
		this.table = table;
	}
	
	public api_createOderTable(int status, String message) {
		super();
		this.status = status;
		this.message = message;
		this.table = null;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public tbl_oder_table getTable() {
		return table;
	}

	public void setTable(tbl_oder_table table) {
		this.table = table;
	}

}
