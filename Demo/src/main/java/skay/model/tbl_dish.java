package skay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_dish")
public class tbl_dish {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "dish_id")
	private int dishid;

	@Column(name = "dish_name")
	private String dishname;

	@Column(name = "url_image")
	private String url;

	private int price;

	public tbl_dish() {
		super();
	}

	public tbl_dish(String dishname, String url, int price) {
		super();
		this.dishname = dishname;
		this.url = url;
		this.price = price;
	}

	public tbl_dish(int dishid, String dishname, String url, int price) {
		super();
		this.dishid = dishid;
		this.dishname = dishname;
		this.url = url;
		this.price = price;
	}

	public int getDishid() {
		return dishid;
	}

	public void setDishid(int dishid) {
		this.dishid = dishid;
	}

	public String getDishname() {
		return dishname;
	}

	public void setDishname(String dishname) {
		this.dishname = dishname;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

}
