package skay.model;

import java.util.List;

public class api_table {
	private int status;
	private String message;
	private List<tbl_table> result;

	public api_table() {
		super();
	}

	public api_table(int status, String message, List<tbl_table> result) {
		super();
		this.status = status;
		this.message = message;
		this.result = result;
	}

	public api_table(int status, String message) {
		super();
		this.status = status;
		this.message = message;
		this.result = null;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<tbl_table> getResult() {
		return result;
	}

	public void setResult(List<tbl_table> result) {
		this.result = result;
	}

}
