package skay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_table")
public class tbl_table {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer stt;

	@Column(name = "count_people")
	private Integer countpeople;

	private String note;
	private Integer check;

	public tbl_table() {
		super();
	}

	public tbl_table(Integer id, Integer stt, Integer countpeople, String note, Integer check) {
		super();
		this.id = id;
		this.stt = stt;
		this.countpeople = countpeople;
		this.note = note;
		this.check = check;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStt() {
		return stt;
	}

	public void setStt(Integer stt) {
		this.stt = stt;
	}

	public Integer getCountpeople() {
		return countpeople;
	}

	public void setCountpeople(Integer countpeople) {
		this.countpeople = countpeople;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getCheck() {
		return check;
	}

	public void setCheck(Integer check) {
		this.check = check;
	}

}
