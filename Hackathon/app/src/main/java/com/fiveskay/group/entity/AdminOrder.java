package com.fiveskay.group.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Awesome Pojo Generator
 */
public class AdminOrder {
  @SerializedName("oder") @Expose private Order oder;
  @SerializedName("date_time") @Expose private String date_time;
  @SerializedName("oder_detail") @Expose private List<OrderDetail> oder_detail;
  @SerializedName("id") @Expose private Integer id;
  @SerializedName("user") @Expose private User user;
  @SerializedName("table") @Expose private Table table;

  public void setOder(Order oder) {
    this.oder = oder;
  }

  public Order getOder() {
    return oder;
  }

  public void setDate_time(String date_time) {
    this.date_time = date_time;
  }

  public String getDate_time() {
    return date_time;
  }

  public void setOder_detail(List<OrderDetail> oder_detail) {
    this.oder_detail = oder_detail;
  }

  public List<OrderDetail> getOder_detail() {
    return oder_detail;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public User getUser() {
    return user;
  }

  public void setTable(Table table) {
    this.table = table;
  }

  public Table getTable() {
    return table;
  }
}