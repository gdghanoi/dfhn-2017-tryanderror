package com.fiveskay.group.utils;

import io.reactivex.disposables.Disposable;
import java.util.List;

/**
 * Created by lan on 11/18/17.
 */

public final class AppUtils {
  private AppUtils() {
  }

  public static void dispose(List<Disposable> disposables) {
    for(Disposable disposable : disposables) {
      disposable.dispose();
    }
  }
}
