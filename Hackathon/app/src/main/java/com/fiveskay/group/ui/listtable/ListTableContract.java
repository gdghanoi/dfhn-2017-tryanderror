package com.fiveskay.group.ui.listtable;

import com.fiveskay.group.base.BasePresenter;
import com.fiveskay.group.base.BaseView;
import com.fiveskay.group.entity.Table;
import java.util.List;

/**
 * Created by lan on 11/18/17.
 */

public interface ListTableContract {

  interface View extends BaseView {
    void showTableList(List<Table> tableList);
    void showTableStatus(int status);
  }

  interface Presenter extends BasePresenter {
    void getListTable();
    void getTableStatus(int tableId);
  }
}
