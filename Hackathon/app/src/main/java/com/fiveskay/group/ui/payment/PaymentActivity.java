package com.fiveskay.group.ui.payment;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.fiveskay.group.AppConst;
import com.fiveskay.group.R;
import com.fiveskay.group.adapter.DishAdapter;
import com.fiveskay.group.base.BaseActivity;
import com.fiveskay.group.entity.Dish;

import java.util.ArrayList;

import butterknife.BindView;

public class PaymentActivity extends BaseActivity implements DishAdapter.ClickListener {

    @BindView(R.id.list_food_id)
    RecyclerView mList;
    @BindView(R.id.payNow)
    Button mBtn;
    private ArrayList<Dish> mListDish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        mListDish = new ArrayList<>();
        mList.setHasFixedSize(true);
        mList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mListDish= intent.getParcelableArrayListExtra(AppConst.EXTRA_DISH);
        DishAdapter dishAdapter = new DishAdapter(getApplication(), false,  mListDish, this);
        mList.setAdapter(dishAdapter);

        mBtn.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Toast.makeText(PaymentActivity.this, "Payment Succeed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_payment;
    }

    @Nullable
    @Override
    public Toolbar getToolbar() {
        return null;
    }

    @Override
    public boolean showHomeEnable() {
        return false;
    }

    @Override
    public void onItemClick(View v, int position) {
        Toast.makeText(this, R.string.succed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemChangeQuantity(Editable editable, int position) {

    }
}
