package com.fiveskay.group.base;

import android.content.Context;

/**
 * Created by PA on 10/13/2017.
 */

public interface BaseView {
    Context getContext();
}
