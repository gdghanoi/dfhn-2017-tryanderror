package com.fiveskay.group;

import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import com.fiveskay.group.base.BaseActivity;

public class MainActivity extends BaseActivity {

  @Override public int getContentLayout() {
    return R.layout.activity_main;
  }

  @Nullable @Override public Toolbar getToolbar() {
    return null;
  }

  @Override public boolean showHomeEnable() {
    return true;
  }
}
