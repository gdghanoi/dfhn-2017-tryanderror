package com.fiveskay.group.ui.orderconfirm;

import android.util.Log;

import com.fiveskay.group.entity.Order;
import com.fiveskay.group.network.ApiClient;
import com.fiveskay.group.network.ApiService;
import com.fiveskay.group.network.response.OrderTableResponse;
import com.fiveskay.group.ui.order.OrderTableContract;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.fiveskay.group.AppConst.DATE_FORMAT;

/**
 * Created by Administrator on 18/11/2017.
 */

public class OrderTableConfirmPresenter implements OrderConfirmContract.Presenter {
    private ApiService mApi;
    private List<Disposable> mDisposables;
    private OrderConfirmContract.View mView;

    public OrderTableConfirmPresenter(OrderConfirmContract.View view) {
        mView = view;
        mApi = ApiClient.apiService();
        mDisposables = new ArrayList<>();
    }


    @Override
    public void createOrder(int tableId, int userId, String dishcount) {
        String data = DATE_FORMAT.format(Calendar.getInstance().getTime());
        Observable<OrderTableResponse>
                observable = mApi.createTableOrder(tableId, userId, dishcount, data);
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<OrderTableResponse>() {
                    @Override
                    public void accept(OrderTableResponse orderTableResponse) throws Exception {
                        Order order = orderTableResponse.getOrder();
                        Log.d("Create order", orderTableResponse.getMessage());
                        if (orderTableResponse.getStatus() == 1) {
                            mView.successOrder();
                        } else {
                            mView.failedOrder();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                    }
                });

    }

    @Override
    public void destroy() {

    }
}
