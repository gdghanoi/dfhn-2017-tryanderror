package com.fiveskay.group.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 */
public class Dish implements Parcelable {
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("dishid")
    @Expose
    private Integer dishid;
    @SerializedName("dishname")
    @Expose
    private String dishname;
    @SerializedName("url")
    @Expose
    private String url;

    private int quantity;
    private boolean isSelected;

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPrice() {
        return price;
    }

    public void setDishid(Integer dishid) {
        this.dishid = dishid;
    }

    public Integer getDishid() {
        return dishid;
    }

    public void setDishname(String dishname) {
        this.dishname = dishname;
    }

    public String getDishname() {
        return dishname;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    protected Dish(Parcel in) {
        price = in.readByte() == 0x00 ? null : in.readInt();
        dishid = in.readByte() == 0x00 ? null : in.readInt();
        dishname = in.readString();
        url = in.readString();
        quantity = in.readInt();
        isSelected = in.readByte() != 0x00;
    }

    public Dish(Integer price, Integer dishid, String dishname, String url, int quantity, boolean isSelected) {
        this.price = price;
        this.dishid = dishid;
        this.dishname = dishname;
        this.url = url;
        this.quantity = quantity;
        this.isSelected = isSelected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (price == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(price);
        }
        if (dishid == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(dishid);
        }
        dest.writeString(dishname);
        dest.writeString(url);
        dest.writeInt(quantity);
        dest.writeByte((byte) (isSelected ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Dish> CREATOR = new Parcelable.Creator<Dish>() {
        @Override
        public Dish createFromParcel(Parcel in) {
            return new Dish(in);
        }

        @Override
        public Dish[] newArray(int size) {
            return new Dish[size];
        }
    };
}