package com.fiveskay.group.network;

import com.fiveskay.group.network.response.DishListResponse;
import com.fiveskay.group.network.response.LogInResponse;
import com.fiveskay.group.network.response.OrderTableResponse;
import com.fiveskay.group.network.response.TableListResponse;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by lan on 8/26/17.
 */

public interface ApiService {

  @POST("login")
  @FormUrlEncoded
  Observable<LogInResponse> login(@Field("username") String username, @Field("pass") String password);

  @POST("getAllTable")
  Observable<TableListResponse> getListTable();

  @POST("getListDish")
  @FormUrlEncoded
  Observable<DishListResponse> getDishes(@Field("limit") int limit, @Field("index") int index);

  @POST("checkTable")
  @FormUrlEncoded
  Observable<ApiResponse> getTableStatus (@Field("tableid") int tableId);

  @POST("createOder")
  @FormUrlEncoded
  Observable<OrderTableResponse> createTableOrder (@Field("tableid") int tableId, @Field("userid") int userId, @Field("dishcount") String discount, @Field("date") String date);

  

}
