package com.fiveskay.group.ui.loginscreen;

import com.fiveskay.group.base.BasePresenter;
import com.fiveskay.group.base.BaseView;

/**
 * Created by lan on 11/18/17.
 */

public class LoginContract {
  public interface View extends BaseView {
    void showSuccess();
    void showFailure();
  }

  public interface Presenter extends BasePresenter {
    void login(String username, String password);
  }
}
