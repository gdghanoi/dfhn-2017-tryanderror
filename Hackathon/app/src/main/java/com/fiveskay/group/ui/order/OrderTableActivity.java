package com.fiveskay.group.ui.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fiveskay.group.AppConst;
import com.fiveskay.group.R;
import com.fiveskay.group.base.BaseActivity;
import com.fiveskay.group.entity.Dish;
import com.fiveskay.group.entity.Table;
import com.fiveskay.group.ui.custom.DatePickerFragment;
import com.fiveskay.group.ui.custom.TimePickerFragment;
import com.fiveskay.group.ui.dish.DishPickerFragment;
import com.fiveskay.group.ui.listtable.ListTableActivity;
import com.fiveskay.group.ui.orderconfirm.OrderConfirmActivity;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;

/**
 * Created by Administrator on 18/11/2017.
 */

public class OrderTableActivity extends BaseActivity implements OrderTableContract.View, View.OnClickListener,
        DatePickerFragment.OnCompleteListener, TimePickerFragment.OnCompleteListener, DishPickerFragment.OnCompleteListener {

    private OrderTablePresenter mPresenter;

    @BindView(R.id.btn_act_order_table_date)
    Button btnPickDate;
    @BindView(R.id.tv_act_order_table_date)
    TextView tvDate;
    @BindView(R.id.btn_act_order_table_time)
    Button btnPickTime;
    @BindView(R.id.tv_act_order_table_time)
    TextView tvTime;
    @BindView(R.id.btn_act_order_table_food)
    Button btnPickFood;
    @BindView(R.id.btn_act_order_table_map)
    Button btnSeeMap;
    @BindView(R.id.btn_act_order_table_done)
    Button btnContinue;

    private ArrayList<Dish> mDishes;
    private Table mTable;
    private Calendar mCalendar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new OrderTablePresenter(this);
        mTable = getIntent().getParcelableExtra(ListTableActivity.KEY_TABLE_REQUEST);
        mDishes = new ArrayList<>();
        mCalendar = Calendar.getInstance();
        initEvent();
    }

    private void initEvent() {
        btnPickDate.setOnClickListener(this);
        btnPickTime.setOnClickListener(this);
        btnPickFood.setOnClickListener(this);
        btnSeeMap.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_order_table;
    }

    @Nullable
    @Override
    public Toolbar getToolbar() {
        return null;
    }

    @Override
    public boolean showHomeEnable() {
        return false;
    }

    @Override
    public Context getContext() {
        return getContext();
    }

    @Override
    public void showDatePicker() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void showTimePicker() {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void showFoodPicker() {
        DialogFragment newFragment = DishPickerFragment.newInstance(mDishes);
        newFragment.show(getSupportFragmentManager(), "foodPicker");
    }

    @Override
    public void showMap() {

    }

    @Override
    public void openOrderConfirmActivity() {
        Intent intent = new Intent(this, OrderConfirmActivity.class);
        ArrayList<Dish> mDishTemp = new ArrayList<>();
        for (int i = 0; i < mDishes.size(); i++) {
            if (mDishes.get(i).isSelected()) {
                mDishTemp.add(mDishes.get(i));
            }
        }
        intent.putExtra(ListTableActivity.KEY_TABLE_REQUEST, mTable);
        intent.putParcelableArrayListExtra(AppConst.EXTRA_DISH, mDishTemp);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_act_order_table_date:
                showDatePicker();
                break;
            case R.id.btn_act_order_table_time:
                showTimePicker();
                break;
            case R.id.btn_act_order_table_food:
                showFoodPicker();
                break;
            case R.id.btn_act_order_table_map:
                showMap();
                break;
            case R.id.btn_act_order_table_done:
                openOrderConfirmActivity();
                break;
        }
    }

    @Override
    public void onCompleteDate(Calendar calendar) {
        mCalendar.setTimeInMillis(calendar.getTimeInMillis());
        tvDate.setText(AppConst.dateFormat.format(calendar.getTime()));
    }

    @Override
    public void onCompleteTime(Calendar calendar) {
        mCalendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
        mCalendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE));
        tvTime.setText(AppConst.timeFormat.format(calendar.getTime()));
    }

    @Override
    public void onCompleteDish(ArrayList<Dish> dishes) {
        mDishes.addAll(dishes);
    }
}
