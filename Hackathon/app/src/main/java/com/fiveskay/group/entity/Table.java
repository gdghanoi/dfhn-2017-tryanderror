package com.fiveskay.group.entity;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 */
public class Table implements Parcelable {
  @SerializedName("note") @Expose private String note;
  @SerializedName("stt") @Expose private int stt;
  @SerializedName("countpeople") @Expose private int countpeople;
  @SerializedName("id") @Expose private int id;
  @SerializedName("check") @Expose private Integer check;

  public void setNote(String note) {
    this.note = note;
  }

  public String getNote() {
    return note;
  }

  public void setStt(int stt) {
    this.stt = stt;
  }

  public Integer getStt() {
    return stt;
  }

  public void setCountpeople(int countpeople) {
    this.countpeople = countpeople;
  }

  public Integer getCountpeople() {
    return countpeople;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setCheck(int check) {
    this.check = check;
  }

  public Integer getCheck() {
    return check;
  }

  protected Table(Parcel in) {
    note = in.readString();
    stt = in.readInt();
    countpeople = in.readInt();
    id = in.readInt();
    check = in.readByte() == 0x00 ? null : in.readInt();
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(note);
    dest.writeInt(stt);
    dest.writeInt(countpeople);
    dest.writeInt(id);
    if (check == null) {
      dest.writeByte((byte) (0x00));
    } else {
      dest.writeByte((byte) (0x01));
      dest.writeInt(check);
    }
  }

  @SuppressWarnings("unused")
  public static final Parcelable.Creator<Table> CREATOR = new Parcelable.Creator<Table>() {
    @Override
    public Table createFromParcel(Parcel in) {
      return new Table(in);
    }

    @Override
    public Table[] newArray(int size) {
      return new Table[size];
    }
  };
}