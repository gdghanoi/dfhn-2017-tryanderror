package com.fiveskay.group.entity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class User{
  @SerializedName("password")
  @Expose
  private Integer password;
  @SerializedName("role")
  @Expose
  private Integer role;
  @SerializedName("gender")
  @Expose
  private Integer gender;
  @SerializedName("phone")
  @Expose
  private Integer phone;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("userid")
  @Expose
  private Integer userid;
  @SerializedName("username")
  @Expose
  private String username;
  public void setPassword(Integer password){
   this.password=password;
  }
  public Integer getPassword(){
   return password;
  }
  public void setRole(Integer role){
   this.role=role;
  }
  public Integer getRole(){
   return role;
  }
  public void setGender(Integer gender){
   this.gender=gender;
  }
  public Integer getGender(){
   return gender;
  }
  public void setPhone(Integer phone){
   this.phone=phone;
  }
  public Integer getPhone(){
   return phone;
  }
  public void setName(String name){
   this.name=name;
  }
  public String getName(){
   return name;
  }
  public void setUserid(Integer userid){
   this.userid=userid;
  }
  public Integer getUserid(){
   return userid;
  }
  public void setUsername(String username){
   this.username=username;
  }
  public String getUsername(){
   return username;
  }
}