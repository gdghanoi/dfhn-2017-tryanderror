package com.fiveskay.group.ui.custom;

import com.fiveskay.group.entity.Table;

public interface ClickListener {

    void onConfirm(Table table);

}
