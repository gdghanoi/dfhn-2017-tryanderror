package com.fiveskay.group.ui.orderconfirm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fiveskay.group.AppConst;
import com.fiveskay.group.R;
import com.fiveskay.group.base.BaseActivity;
import com.fiveskay.group.entity.Dish;
import com.fiveskay.group.entity.Table;
import com.fiveskay.group.entity.User;
import com.fiveskay.group.helper.DataHelper;
import com.fiveskay.group.helper.impl.DataHelperImpl;
import com.fiveskay.group.ui.listtable.ListTableActivity;
import com.fiveskay.group.ui.payment.PaymentActivity;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by Administrator on 18/11/2017.
 */

public class OrderConfirmActivity extends BaseActivity
        implements OrderConfirmContract.View, View.OnClickListener {

    @BindView(R.id.btn_order_confirm_edit)
    Button btnEdit;
    @BindView(R.id.tv_order_confirm_date)
    TextView tvDate;
    @BindView(R.id.tv_order_confirm_time)
    TextView tvTime;
    @BindView(R.id.tv_order_confirm_address)
    TextView tvDescription;
    @BindView(R.id.tv_order_confirm_total)
    TextView tvTotal;
    @BindView(R.id.edt_order_confirm_name)
    EditText edtName;
    @BindView(R.id.edt_order_confirm_email)
    EditText edtEmail;
    @BindView(R.id.edt_order_confirm_phone)
    EditText edtPhone;
    @BindView(R.id.btn_order_confirm_continue)
    Button btnContinue;

    private ArrayList<Dish> mDishes;
    private StringBuffer mStringDish;
    private Table mTable;
    OrderTableConfirmPresenter mPresenter;

    @Override
    public Context getContext() {
        return getContext();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStringDish = new StringBuffer();
        mDishes = getIntent().getParcelableArrayListExtra(AppConst.EXTRA_DISH);
        mTable = getIntent().getParcelableExtra(ListTableActivity.KEY_TABLE_REQUEST);
        mPresenter = new OrderTableConfirmPresenter(this);
        initEvent();
    }

    private void initEvent() {
        btnEdit.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
    }

    @Override
    public int getContentLayout() {
        return R.layout.activity_order_confirm;
    }

    @Nullable
    @Override
    public Toolbar getToolbar() {
        return null;
    }

    @Override
    public boolean showHomeEnable() {
        return false;
    }


    @Override
    public void openOrder() {
        finish();
    }

    @Override
    public void openPayment() {

    }

    @Override
    public void successOrder() {
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putParcelableArrayListExtra(AppConst.EXTRA_DISH, mDishes);
        startActivity(intent);
    }

    @Override
    public void failedOrder() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_order_confirm_continue:
                for (int i = 0; i < mDishes.size(); i++) {
                    if (i != 0) mStringDish.append("-");
                    mStringDish.append(mDishes.get(i).getDishid() + "," + mDishes.get(i).getQuantity());
                }
                DataHelper dataHelper = new DataHelperImpl();
                User user = dataHelper.getLoginUser();

                Log.d("asd", "onClick: " + dataHelper.getLoginUser().getUserid());
                mPresenter.createOrder(mTable.getId(), dataHelper.getLoginUser().getUserid(), mStringDish.toString());
//                Intent intent = new Intent(this, PaymentActivity.class);
//                intent.putParcelableArrayListExtra(AppConst.EXTRA_DISH, mDishes);
//                startActivity(intent);
                break;
            case R.id.btn_order_confirm_edit:
                openOrder();
                break;
        }
    }
}
