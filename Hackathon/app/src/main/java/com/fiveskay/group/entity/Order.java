package com.fiveskay.group.entity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Order{
  @SerializedName("oderdate")
  @Expose
  private String oderdate;
  @SerializedName("price")
  @Expose
  private Integer price;
  @SerializedName("oderid")
  @Expose
  private Integer oderid;
  @SerializedName("tableid")
  @Expose
  private Integer tableid;
  @SerializedName("userid")
  @Expose
  private Integer userid;
  public void setOderdate(String oderdate){
   this.oderdate=oderdate;
  }
  public String getOderdate(){
   return oderdate;
  }
  public void setPrice(Integer price){
   this.price=price;
  }
  public Integer getPrice(){
   return price;
  }
  public void setOderid(Integer oderid){
   this.oderid=oderid;
  }
  public Integer getOderid(){
   return oderid;
  }
  public void setTableid(Integer tableid){
   this.tableid=tableid;
  }
  public Integer getTableid(){
   return tableid;
  }
  public void setUserid(Integer userid){
   this.userid=userid;
  }
  public Integer getUserid(){
   return userid;
  }
}