package com.fiveskay.group.ui.infotable;

import com.fiveskay.group.base.BasePresenter;
import com.fiveskay.group.base.BaseView;

/**
 * Created by lan on 11/18/17.
 */

public interface TableInfoContract {

  interface View extends BaseView {

  }

  interface Presenter extends BasePresenter {

  }
}
