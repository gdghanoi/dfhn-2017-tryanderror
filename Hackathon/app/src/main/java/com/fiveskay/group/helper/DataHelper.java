package com.fiveskay.group.helper;

import com.fiveskay.group.entity.User;

/**
 * Created by lan on 11/18/17.
 */

public interface DataHelper {
  void saveLoginUser(User user);
  User getLoginUser();
}
