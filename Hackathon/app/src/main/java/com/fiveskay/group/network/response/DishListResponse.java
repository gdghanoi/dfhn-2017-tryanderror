package com.fiveskay.group.network.response;

import com.fiveskay.group.entity.Dish;
import com.fiveskay.group.network.ApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by lan on 11/18/17.
 */

public class DishListResponse extends ApiResponse {
  @Expose @SerializedName("result") private List<Dish> result;

  public List<Dish> getResult() {
    return result;
  }

  public void setResult(List<Dish> result) {
    this.result = result;
  }
}
