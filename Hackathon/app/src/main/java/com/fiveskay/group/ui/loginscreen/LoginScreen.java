package com.fiveskay.group.ui.loginscreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.OnClick;
import com.fiveskay.group.R;
import com.fiveskay.group.base.BaseActivity;
import com.fiveskay.group.ui.listtable.ListTableActivity;

public class LoginScreen extends BaseActivity implements LoginContract.View {
  private LoginContract.Presenter mPresenter;

  @BindView(R.id.username_et) EditText mUsernameEt;
  @BindView(R.id.password_et) EditText mPasswordEt;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mPresenter = new LoginPresenter(this);
  }

  @Override public int getContentLayout() {
    return R.layout.login_screen;
  }

  @Nullable @Override public Toolbar getToolbar() {
    return null;
  }

  @Override public boolean showHomeEnable() {
    return false;
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    mPresenter.destroy();
  }

  /**
   * View event
   */
  @OnClick(R.id.login_bt) void clickLogin() {
    if (TextUtils.isEmpty(mUsernameEt.getText()) && TextUtils.isEmpty(mPasswordEt.getText())) {
      Toast.makeText(this, R.string.login_warning, Toast.LENGTH_SHORT).show();
      return;
    }

    mPresenter.login(mUsernameEt.getText().toString(), mPasswordEt.getText().toString());
  }

  /**
   * View implement
   */
  @Override public Context getContext() {
    return this;
  }

  @Override public void showSuccess() {
    Toast.makeText(this, R.string.login_success, Toast.LENGTH_SHORT).show();
    Intent intent = new Intent(this, ListTableActivity.class);
    startActivity(intent);
  }

  @Override public void showFailure() {
    Toast.makeText(this, R.string.login_error, Toast.LENGTH_SHORT).show();
  }
}
