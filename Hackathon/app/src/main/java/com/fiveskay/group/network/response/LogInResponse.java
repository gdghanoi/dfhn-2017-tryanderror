package com.fiveskay.group.network.response;

import com.fiveskay.group.entity.User;
import com.fiveskay.group.network.ApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 */
public class LogInResponse extends ApiResponse {
  @SerializedName("user") @Expose private User user;


  public void setUser(User user) {
    this.user = user;
  }

  public User getUser() {
    return user;
  }


}