package com.fiveskay.group.ui.order;

import android.util.Log;
import com.fiveskay.group.entity.Order;
import com.fiveskay.group.network.ApiClient;
import com.fiveskay.group.network.ApiService;
import com.fiveskay.group.network.response.OrderTableResponse;
import com.fiveskay.group.utils.AppUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.fiveskay.group.AppConst.DATE_FORMAT;

/**
 * Created by Administrator on 18/11/2017.
 */

public class OrderTablePresenter implements OrderTableContract.Presenter {
  private OrderTableContract.View mView;
  private ApiService mApi;
  private List<Disposable> mDisposables;

  public OrderTablePresenter(OrderTableContract.View view) {
    mApi = ApiClient.apiService();
    mView = view;
    mDisposables = new ArrayList<>();
  }

  @Override public void createOrder(int tableId, int userId, String dishcount) {
    String data = DATE_FORMAT.format(Calendar.getInstance().getTime());
    Observable<OrderTableResponse> observable = mApi.createTableOrder(tableId, userId, dishcount, data);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<OrderTableResponse>() {
          @Override public void accept(OrderTableResponse orderTableResponse) throws Exception {
            Order order = orderTableResponse.getOrder();
            Log.d("Create order", orderTableResponse.getMessage());

          }
        }, new Consumer<Throwable>() {
          @Override public void accept(Throwable throwable) throws Exception {

          }
        });

  }

  @Override public void destroy() {
    AppUtils.dispose(mDisposables);
  }
}
