package com.fiveskay.group.network;

import com.fiveskay.group.BuildConfig;
import com.google.gson.Gson;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by lan on 8/26/17.
 */

public final class ApiClient {

  public static final int CONNECT_TIMEOUT = 60;
  public static final int READ_TIMEOUT = 60;
  public static final int WRITE_TIMEOUT = 60;

  private static final String BASE_URL = BuildConfig.BASE_URL;

  private ApiClient() {
  }

  public static OkHttpClient httpClient() {
    HttpLoggingInterceptor logging =
        new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
          @Override public void log(String message) {
            Timber.tag("OkHttp").d(message);
          }
        });
    logging.setLevel(HttpLoggingInterceptor.Level.BODY);

    OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging)
        .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
        .build();

    return client;
  }

  public static Retrofit retrofit() {
    Retrofit retrofit = new Retrofit.Builder().client(httpClient())
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create(new Gson()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build();

    return retrofit;
  }

  public static ApiService apiService() {
    return retrofit().create(ApiService.class);
  }
}
