package com.fiveskay.group.admin.order;

import com.fiveskay.group.entity.Order;
import java.util.List;

/**
 * Created by lan on 11/18/17.
 */

public interface OrderListContract {
  interface View {
    void showOrderList(List<Order> orderList);
  }

  interface Presenter {
    void getOrderList();
  }
}
