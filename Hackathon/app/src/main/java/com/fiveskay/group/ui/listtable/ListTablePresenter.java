package com.fiveskay.group.ui.listtable;

import com.fiveskay.group.entity.Table;
import com.fiveskay.group.network.ApiClient;
import com.fiveskay.group.network.ApiResponse;
import com.fiveskay.group.network.ApiService;
import com.fiveskay.group.network.response.TableListResponse;
import com.fiveskay.group.ui.LoadingDialog;
import com.fiveskay.group.utils.AppUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import timber.log.Timber;

/**
 * Created by lan on 11/18/17.
 */

public class ListTablePresenter implements ListTableContract.Presenter {
  private ListTableContract.View mView;
  private List<Disposable> mDisposables;
  private ApiService mApi;

  public ListTablePresenter(ListTableContract.View view) {
    mView = view;
    mDisposables = new ArrayList<>();
    mApi = ApiClient.apiService();
  }

  @Override public void getListTable() {
    final LoadingDialog dialog = new LoadingDialog(mView.getContext());
    dialog.show();

    Observable<TableListResponse> observable = mApi.getListTable();
    Disposable disposable = observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<TableListResponse>() {
          @Override public void accept(TableListResponse tableListResponse) throws Exception {
            dialog.dismiss();

            List<Table> tables = tableListResponse.getResult();
            Timber.w("Get table list: " + tables.size());
            mView.showTableList(tables);
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(Throwable throwable) throws Exception {
            //throw new Exception(throwable);
            dialog.dismiss();
          }
        });
    mDisposables.add(disposable);
  }

  @Override public void getTableStatus(int tableId) {
    final LoadingDialog dialog = new LoadingDialog(mView.getContext());
    dialog.show();

    Observable<ApiResponse> observable = mApi.getTableStatus(tableId);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<ApiResponse>() {
          @Override public void accept(ApiResponse apiResponse) throws Exception {
            dialog.dismiss();
            int status = apiResponse.getStatus();
            mView.showTableStatus(status);
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(Throwable throwable) throws Exception {
            dialog.show();
            //throw new Exception(throwable);
          }
        });
  }

  @Override public void destroy() {
    AppUtils.dispose(mDisposables);
  }
}
