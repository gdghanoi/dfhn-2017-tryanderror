package com.fiveskay.group.admin.order;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import com.fiveskay.group.R;
import com.fiveskay.group.base.BaseActivity;

public class OrderListActivity extends BaseActivity {

  @Override public int getContentLayout() {
    return R.layout.activity_order_list;
  }

  @Nullable @Override public Toolbar getToolbar() {
    return null;
  }

  @Override public boolean showHomeEnable() {
    return false;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }
}
