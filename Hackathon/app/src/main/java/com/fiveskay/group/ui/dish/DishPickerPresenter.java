package com.fiveskay.group.ui.dish;

import com.fiveskay.group.entity.Dish;
import com.fiveskay.group.network.ApiClient;
import com.fiveskay.group.network.ApiService;
import com.fiveskay.group.network.response.DishListResponse;
import com.fiveskay.group.utils.AppUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lan on 11/18/17.
 */

public class DishPickerPresenter implements DishPickerContract.Presenter {
  private List<Disposable> mDisposables;
  private DishPickerContract.View mView;
  private ApiService mApi;

  public DishPickerPresenter(DishPickerContract.View view) {
    mApi = ApiClient.apiService();
    mDisposables = new ArrayList<>();
    mView = view;
  }

  @Override public void destroy() {
    AppUtils.dispose(mDisposables);
  }

  @Override public void getDishes(int limit, int index) {
    Observable<DishListResponse> observable = mApi.getDishes(limit, index);
    observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<DishListResponse>() {
          @Override public void accept(DishListResponse dishListResponse) throws Exception {
            List<Dish> dishes = dishListResponse.getResult();
            mView.showDishes(dishes);
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(Throwable throwable) throws Exception {
            throw new Exception(throwable);
          }
        });
  }
}
