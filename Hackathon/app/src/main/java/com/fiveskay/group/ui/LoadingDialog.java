package com.fiveskay.group.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import butterknife.ButterKnife;
import com.fiveskay.group.R;

/**
 * Created by lan on 9/21/17.
 */

public class LoadingDialog extends Dialog {
  public LoadingDialog(@NonNull Context context) {
    super(context);
    init();
  }

  public LoadingDialog(@NonNull Context context, @StyleRes int themeResId) {
    super(context, themeResId);
    init();
  }

  protected LoadingDialog(@NonNull Context context, boolean cancelable,
      @Nullable OnCancelListener cancelListener) {
    super(context, cancelable, cancelListener);
    init();
  }

  private void init() {
    setCancelable(false);
    getWindow().addFlags(Window.FEATURE_NO_TITLE);
    getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

    //Set content view
    View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_loading, null);
    ButterKnife.bind(this, v);
    setContentView(v);
  }
}
