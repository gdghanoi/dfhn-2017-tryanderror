package com.fiveskay.group.network.response;

import com.fiveskay.group.entity.Order;
import com.fiveskay.group.entity.OrderDetail;
import com.fiveskay.group.network.ApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by lan on 11/18/17.
 */

public class OrderTableResponse extends ApiResponse {
  @Expose @SerializedName("oder") private Order order;
  @Expose @SerializedName("detailOder") private List<OrderDetail> detailOder;

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public List<OrderDetail> getDetailOder() {
    return detailOder;
  }

  public void setDetailOder(List<OrderDetail> detailOder) {
    this.detailOder = detailOder;
  }
}
