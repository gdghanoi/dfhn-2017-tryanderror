package com.fiveskay.group.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lan on 11/18/17.
 */

public class ApiResponse {

  @Expose @SerializedName("status") private int status;
  @Expose @SerializedName("message") private String message;

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
