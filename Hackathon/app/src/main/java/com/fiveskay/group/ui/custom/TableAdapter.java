package com.fiveskay.group.ui.custom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fiveskay.group.R;
import com.fiveskay.group.entity.Table;

import java.util.ArrayList;

/**
 * Created by Admin on 11/18/2017.
 */

public class TableAdapter extends BaseAdapter {

    private ArrayList<Table> mListTable = new ArrayList<Table>();

    private Context mContext;

    public TableAdapter(Context c,  ArrayList<Table> mListTable) {
        mContext = c;
        this.mListTable = mListTable;
    }

    public int getCount() {
        return mListTable.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @SuppressLint("ResourceType")
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView view;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            view = new TextView(mContext);
            view.setLayoutParams(new GridView.LayoutParams(150 , 150));
            view.setPadding(8, 8, 8, 8);
        } else {
            view = (TextView) convertView;
        }

        view.setTextColor(Color.BLACK);
        view.setGravity(Gravity.CENTER);

        Table table = mListTable.get(position);

        if (table == null) {
            return view;
        }

        switch (table.getCheck()){
            case 1:
                //Inavailable
                view.setBackgroundColor(Color.parseColor(mContext.getString(R.color.table_inavailable)));
                break;
            case 2:
                //Available
                view.setBackgroundColor(Color.parseColor(mContext.getString(R.color.table_available)));
                break;
            case 3:
                //Selected
                view.setBackgroundColor(Color.parseColor(mContext.getString(R.color.table_checked)));
                break;
            default:
                break;
        }
        view.setText("" + table.getStt());
//        imageView.setImageResource(mListTable.get(position));
        return view;
    }
}
