package com.fiveskay.group.ui.dish;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fiveskay.group.R;
import com.fiveskay.group.adapter.DishAdapter;
import com.fiveskay.group.entity.Dish;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DishPickerFragment extends DialogFragment
        implements DishPickerContract.View, DishAdapter.ClickListener, View.OnClickListener {

    public static final String TAG = DishPickerFragment.class.getSimpleName();
    private DishPickerContract.Presenter mPresenter;

    @BindView(R.id.rv_fragment_dish)
    RecyclerView rcvDish;
    @BindView(R.id.btn_dish_picker)
    Button btnPickDone;

    private ArrayList<Dish> mDishes;
    private DishAdapter dishAdapter;

    private OnCompleteListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mListener = (OnCompleteListener) context;
    }

    //    // TODO: Rename and change types and number of parameters
    public static DishPickerFragment newInstance(ArrayList<Dish> dishes) {
        DishPickerFragment fragment = new DishPickerFragment();
        fragment.mDishes = new ArrayList<>();
        if (dishes.size() != 0) {
            fragment.mDishes.clear();
            fragment.mDishes.addAll(dishes);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new DishPickerPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_dish_picker, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dishAdapter = new DishAdapter(getContext(), true, mDishes, this);
        rcvDish.setHasFixedSize(true);
        rcvDish.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rcvDish.setAdapter(dishAdapter);
        if (mDishes.size() == 0)
            mPresenter.getDishes(5, 0);
        btnPickDone.setOnClickListener(this);
    }

    @Override
    public void showDishes(List<Dish> dishes) {
        mDishes.addAll(dishes);
        dishAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(View v, int position) {
        mDishes.get(position).setSelected(!mDishes.get(position).isSelected());
        dishAdapter.notifyItemChanged(position);
    }

    @Override
    public void onItemChangeQuantity(Editable editable, int position) {
        Log.d(TAG, "onItemChangeQuantity: " + editable.toString());
        mDishes.get(position).setQuantity(Integer.parseInt(editable.toString()));
    }

    @Override
    public void onClick(View view) {
        mListener.onCompleteDish(mDishes);
        dismiss();
    }

    public interface OnCompleteListener {
        void onCompleteDish(ArrayList<Dish> dishes);
    }
}
