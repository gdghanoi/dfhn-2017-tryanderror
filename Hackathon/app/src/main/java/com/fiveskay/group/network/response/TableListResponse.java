package com.fiveskay.group.network.response;

import com.fiveskay.group.entity.Table;
import com.fiveskay.group.network.ApiResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by lan on 11/18/17.
 */

public class TableListResponse extends ApiResponse {

  @Expose @SerializedName("result") private List<Table> result;

  public List<Table> getResult() {
    return result;
  }

  public void setResult(List<Table> result) {
    this.result = result;
  }
}
