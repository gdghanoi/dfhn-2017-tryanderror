package com.fiveskay.group.ui.listtable;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import butterknife.BindView;
import com.fiveskay.group.R;
import com.fiveskay.group.adapter.DishAdapter;
import com.fiveskay.group.base.BaseActivity;
import com.fiveskay.group.entity.Table;
import com.fiveskay.group.entity.User;
import com.fiveskay.group.helper.DataHelper;
import com.fiveskay.group.helper.impl.DataHelperImpl;
import com.fiveskay.group.ui.custom.ClickListener;
import com.fiveskay.group.ui.custom.CustomDialogClass;
import com.fiveskay.group.ui.custom.TableAdapter;
import com.fiveskay.group.ui.order.OrderTableActivity;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import timber.log.Timber;

public class ListTableActivity extends BaseActivity implements ListTableContract.View, ClickListener {

    @BindView(R.id.my_toolbar) Toolbar mToolbar;
    @BindView(R.id.grid_view) GridView gridview;
    private ListTableContract.Presenter mPresenter;
    private Table mCurrentTable;
    public static final Integer STATUS_AVAILABLE  = 2;
    public static final Integer STATUS_UNAVAILABLE  = 1;
    public static final Integer STATUS_SELECTED  = 3;

    private static final String TAG = "ListTableActivity";
    public static final String KEY_TABLE_REQUEST = "key_table_request";

    @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ListTablePresenter(this);
        mPresenter.getListTable();
    }

    @Override public int getContentLayout() {
        return (R.layout.activity_list_table);
    }

    @Nullable @Override public Toolbar getToolbar() {
        return mToolbar;
    }

    @Override public boolean showHomeEnable() {
        return true;
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        mPresenter.destroy();


    }

    /**
     * View implement
     */
    @Override public void showTableList(final List<Table> tableList) {

        gridview.setAdapter(new TableAdapter(this, (ArrayList<Table>) tableList));

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint({"ResourceAsColor", "SetTextI18n"})
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                //Todo: request order table
                final Table table =  tableList.get(position);

                Log.d(TAG, "onItemClick: "+table.getCheck());
                if (Objects.equals(table.getCheck(), STATUS_AVAILABLE)){
                    showDialogRequest(table);
                }
            }
        });
    }

    private void showDialogRequest(Table table){
        mCurrentTable = table;
        CustomDialogClass cdd = new CustomDialogClass( ListTableActivity.this, table );
        cdd.mListener = this;
        cdd.show();
    }


    @Override public void showTableStatus(int status) {
        if (status == STATUS_AVAILABLE && mCurrentTable != null ){
            Intent intent = new Intent(this, OrderTableActivity.class);
            intent.putExtra(KEY_TABLE_REQUEST, mCurrentTable);
            startActivity(intent);
        }else{
            mPresenter.getListTable();
        }
    }

    @Override
    public void onConfirm(Table table) {
        if (mPresenter != null){
            mPresenter.getTableStatus(table.getCheck());
        }
    }

    @Override public Context getContext() {
        return this;
    }
}
