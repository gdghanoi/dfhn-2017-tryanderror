package com.fiveskay.group.entity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class OrderDetail{
  @SerializedName("oder_detail_id")
  @Expose
  private Integer oder_detail_id;
  @SerializedName("price")
  @Expose
  private Integer price;
  @SerializedName("oderid")
  @Expose
  private Integer oderid;
  @SerializedName("dishid")
  @Expose
  private Integer dishid;
  @SerializedName("count")
  @Expose
  private Integer count;
  public void setOder_detail_id(Integer oder_detail_id){
   this.oder_detail_id=oder_detail_id;
  }
  public Integer getOder_detail_id(){
   return oder_detail_id;
  }
  public void setPrice(Integer price){
   this.price=price;
  }
  public Integer getPrice(){
   return price;
  }
  public void setOderid(Integer oderid){
   this.oderid=oderid;
  }
  public Integer getOderid(){
   return oderid;
  }
  public void setDishid(Integer dishid){
   this.dishid=dishid;
  }
  public Integer getDishid(){
   return dishid;
  }
  public void setCount(Integer count){
   this.count=count;
  }
  public Integer getCount(){
   return count;
  }
}