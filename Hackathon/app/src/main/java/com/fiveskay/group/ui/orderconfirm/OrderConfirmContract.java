package com.fiveskay.group.ui.orderconfirm;

import com.fiveskay.group.base.BasePresenter;
import com.fiveskay.group.base.BaseView;

/**
 * Created by Administrator on 18/11/2017.
 */

public class OrderConfirmContract {

    interface View extends BaseView {
        void openOrder();
        void openPayment();
        void successOrder();
        void failedOrder();
    }

    interface Presenter extends BasePresenter {
        void createOrder(int tableId, int userId, String dishcount);
    }
}
