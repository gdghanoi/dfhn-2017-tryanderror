package com.fiveskay.group.ui.dish;

import com.fiveskay.group.base.BasePresenter;
import com.fiveskay.group.entity.Dish;

import java.util.List;

/**
 * Created by lan on 11/18/17.
 */

public interface DishPickerContract {
    interface View {
        void showDishes(List<Dish> dishes);
    }

    interface Presenter extends BasePresenter {
        void getDishes(int limit, int index);
    }
}
