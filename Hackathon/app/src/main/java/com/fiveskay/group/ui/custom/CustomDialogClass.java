package com.fiveskay.group.ui.custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.fiveskay.group.R;
import com.fiveskay.group.entity.Table;

/**
 * Created by Admin on 11/18/2017.
 */

public class CustomDialogClass extends Dialog implements
        android.view.View.OnClickListener {

    public Context c;
    public Dialog d;
    public Button yes, no;
    public Table table;
    private TextView mTxtIcon, mTxtDescipsion;
    public ClickListener mListener;

    public CustomDialogClass(Activity a, Table table) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.table = table;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custorm_dialog);
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        mTxtDescipsion = (TextView) findViewById(R.id.txt_descibsion);
        mTxtIcon = (TextView) findViewById(R.id.txt_dia);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);

        if (table == null) {
            return;
        }
        mTxtIcon.setText(String.valueOf(table.getStt()));
        Spanned describe = Html.fromHtml(table.getNote());
        mTxtDescipsion.setText(describe);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
                //Todo:
                if (mListener != null){
                    mListener.onConfirm(table);
                }
                break;
            case R.id.btn_no:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}

