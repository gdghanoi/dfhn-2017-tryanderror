package com.fiveskay.group.ui.loginscreen;

import com.fiveskay.group.entity.User;
import com.fiveskay.group.helper.DataHelper;
import com.fiveskay.group.helper.impl.DataHelperImpl;
import com.fiveskay.group.network.ApiClient;
import com.fiveskay.group.network.ApiService;
import com.fiveskay.group.network.response.LogInResponse;
import com.fiveskay.group.ui.LoadingDialog;
import com.fiveskay.group.utils.AppUtils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import timber.log.Timber;

/**
 * Created by lan on 11/18/17.
 */

public class LoginPresenter implements LoginContract.Presenter {
  private LoginContract.View mView;
  private ApiService mApi;

  private List<Disposable> mDisposables;

  public LoginPresenter(LoginContract.View view) {
    mApi = ApiClient.apiService();
    mView = view;
    mDisposables = new ArrayList<>();
  }

  @Override public void login(String username, String password) {
    Timber.w("Run login");
    //mView.showProgress();

    final LoadingDialog dialog = new LoadingDialog(mView.getContext());
    dialog.show();

    Observable<LogInResponse> observable = mApi.login(username, password);
    Disposable disposable = observable.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Consumer<LogInResponse>() {
          @Override public void accept(LogInResponse logInResponse) throws Exception {
            dialog.dismiss();

            User user = logInResponse.getUser();
            DataHelper dataHelper = new DataHelperImpl();
            dataHelper.saveLoginUser(user);

            if (user != null) {
              mView.showSuccess();
            } else {
              mView.showFailure();
            }
          }
        }, new Consumer<Throwable>() {
          @Override public void accept(Throwable throwable) throws Exception {
            dialog.dismiss();

            mView.showFailure();
            //throw new Exception(throwable);
          }
        });

    mDisposables.add(disposable);
  }

  @Override public void destroy() {
    AppUtils.dispose(mDisposables);
  }
}
