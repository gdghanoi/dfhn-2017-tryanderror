package com.fiveskay.group;

import android.app.Application;
import io.paperdb.Paper;
import timber.log.Timber;

/**
 * Created by lan on 11/18/17.
 */

public class MyApplication extends Application {
  @Override public void onCreate() {
    super.onCreate();
    Paper.init(this);

    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
    }
  }
}
