package com.fiveskay.group.helper.impl;

import com.fiveskay.group.entity.User;
import com.fiveskay.group.helper.DataHelper;
import io.paperdb.Paper;

/**
 * Created by lan on 11/18/17.
 */

public class DataHelperImpl implements DataHelper {
  private static final String USER_LOGIN_KEY = "user_login";

  public DataHelperImpl() {
  }

  @Override public void saveLoginUser(User user) {
    Paper.book().write(USER_LOGIN_KEY, user);
  }

  @Override public User getLoginUser() {
    return Paper.book().read(USER_LOGIN_KEY, null);
  }
}
