package com.fiveskay.group.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import butterknife.ButterKnife;

/**
 * Created by PA on 10/19/2017.
 */

public abstract class BaseActivity extends AppCompatActivity {
  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(getContentLayout());
    ButterKnife.bind(this);

    if (getToolbar() != null) {
      setSupportActionBar(getToolbar());
    }
    if (getSupportActionBar() != null) {
      getSupportActionBar().setDisplayHomeAsUpEnabled(showHomeEnable());
      getSupportActionBar().setDisplayShowHomeEnabled(showHomeEnable());
    }
  }

  @LayoutRes public abstract int getContentLayout();

  @Nullable public abstract Toolbar getToolbar();

  public abstract boolean showHomeEnable();

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == android.R.id.home) {
      onBackPressed();

      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
