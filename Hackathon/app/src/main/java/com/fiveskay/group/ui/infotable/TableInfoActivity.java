package com.fiveskay.group.ui.infotable;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import com.fiveskay.group.R;
import com.fiveskay.group.base.BaseActivity;

public class TableInfoActivity extends BaseActivity implements TableInfoContract.View {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public int getContentLayout() {
    return R.layout.activity_table_info;
  }

  @Nullable @Override public Toolbar getToolbar() {
    return null;
  }

  @Override public boolean showHomeEnable() {
    return false;
  }

  /**
   * View implement
   */
  @Override public Context getContext() {
    return this;
  }
}
