package com.fiveskay.group;

import java.text.SimpleDateFormat;

/**
 * Created by lan on 11/18/17.
 */

public class AppConst {
    private AppConst() {
    }

  public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public interface NetworkConst {
        int STATUS_SUCCESS = 1;

    }

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm");

    public static final String EXTRA_TABLE_ID = "tableId";
    public static final String EXTRA_TABLE_DESCRIPTION = "tableDescription";
    public static final String EXTRA_DISH = "listdishes";
    public static final String EXTRA_TIME = "time";
}
