package com.fiveskay.group.ui.order;

import com.fiveskay.group.base.BasePresenter;
import com.fiveskay.group.base.BaseView;

/**
 * Created by Administrator on 18/11/2017.
 */

public interface OrderTableContract {

    interface View extends BaseView {
        void showDatePicker();
        void showTimePicker();
        void showFoodPicker();
        void showMap();
        void openOrderConfirmActivity();
    }

    interface Presenter extends BasePresenter {
        void createOrder(int tableId, int userId, String dishcount);
    }
}
