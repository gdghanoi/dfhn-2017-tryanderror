package com.fiveskay.group.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.fiveskay.group.R;
import com.fiveskay.group.entity.Dish;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Administrator on 18/11/2017.
 */

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.ItemRowHolder> {

    private Context mContext;
    private ArrayList<Dish> mDishes;
    private ClickListener onClickListener;
    private boolean mIsEdit;

    public DishAdapter(Context context, boolean isEdit, ArrayList<Dish> dishes, ClickListener onClickListener) {
        this.mContext = context;
        this.mIsEdit = isEdit;
        this.mDishes = dishes;
        this.onClickListener = onClickListener;
    }

    @Override
    public ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dish_cell, null);
        ItemRowHolder mh = new ItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(ItemRowHolder holder, int position) {
        Dish dish = mDishes.get(position);
        Log.d("asd", "onBindViewHolder: " + dish.getUrl());
        Glide.with(mContext)
                .load(dish.getUrl())
                .into(holder.ivThumb);
        holder.tvName.setText(dish.getDishname());
        if (mIsEdit) {
            holder.edtQuantity.setVisibility(View.VISIBLE);
            holder.tvTotal.setVisibility(View.GONE);
            holder.cbSelected.setVisibility(View.VISIBLE);
            holder.edtQuantity.setText(String.valueOf(dish.getQuantity()));
            holder.cbSelected.setChecked(dish.isSelected());
        } else {
            holder.edtQuantity.setVisibility(View.GONE);
            holder.tvTotal.setVisibility(View.VISIBLE);
            holder.cbSelected.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mDishes.size();
    }

    public class ItemRowHolder extends RecyclerView.ViewHolder implements View.OnClickListener, TextWatcher {

        private ImageView ivThumb;
        private TextView tvName;
        private TextView tvTotal;
        private EditText edtQuantity;
        private CheckBox cbSelected;

        ItemRowHolder(View view) {
            super(view);
            this.ivThumb = view.findViewById(R.id.iv_dish_cell);
            this.tvName = view.findViewById(R.id.tv_dish_cell);
            this.tvTotal = view.findViewById(R.id.tv_dish_total);
            this.edtQuantity = view.findViewById(R.id.edt_dish_cell_quantity);
            this.cbSelected = view.findViewById(R.id.cb_dish_cell);
            view.setOnClickListener(this);
            edtQuantity.addTextChangedListener(this);
        }

        @Override
        public void onClick(View view) {
            onClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            if (!TextUtils.isEmpty(editable.toString()))
                onClickListener.onItemChangeQuantity(editable, getAdapterPosition());
        }
    }

    public interface ClickListener {
        void onItemClick(View v, int position);
        void onItemChangeQuantity(Editable editable, int position);
    }
}
